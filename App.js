import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// Stack navigator for react-navigate
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

//imports screens (routes)
import CreateAppointment from './screens/create-appointment/create-appointment';
import ListAppointments from './screens/list-appointment/list-appointment';



// stack instance for implements methods of stack
const Stack = createStackNavigator();

function App() {
  return (
    //main navigation
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="MedicalGo" component={ListAppointments} />
        <Stack.Screen name="Crear Cita" component={CreateAppointment} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});

export default App;