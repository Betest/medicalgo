import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TouchableHighlight, FlatList } from 'react-native-gesture-handler';

//import component
import CardComponent from './card-component';
import { useIsFocused } from '@react-navigation/native';


function ListAppointments({navigation}){
    const isFocused = useIsFocused();
    const [Data, setData] = useState([]);
    
    //res data for flatlist form api
    const fetchAppointments = async () => {
        try{
          let response = await fetch('http://192.168.1.60:3000/api/listAppointments');
          let jsonRes = await response.json();
          //console.log(jsonRes);
          setData(jsonRes.appointments);
        }catch(error){
          console.log('error');
        }                
    }
    useEffect(()=>{
        fetchAppointments();
    },[isFocused]);

    //Elimina pacientes del state
    const deleteAppointment = _id => {
      setData((citasActuales)=>{
        return citasActuales.filter( appointment => appointment._id !== _id);
      })
    }
    
   
    return(        

        <View style={styles.container}>

            <TouchableHighlight style={styles.createAppointmentButton} onPress={()=>navigation.navigate('Crear Cita')}>

                <Text style={styles.buttonTextStyle}>Crear Cita Medica</Text>

            </TouchableHighlight>

            <Text style={styles.title}>{Data.length > 0 ? 'Administra tus citas': 'No hay citas agendadas'}</Text>

            <FlatList 
              data={Data}
              renderItem={({ item })=> <CardComponent appointment={item} deleteAppointment={deleteAppointment}/>}
              keyExtractor={appointment => appointment._id}            
            >

            </FlatList>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column'
    },
    createAppointmentButton: {
      alignItems: 'center',
      backgroundColor: 'orange',
      padding: 20,
      borderRadius: 50,
      marginVertical: 8,
      marginHorizontal: 100
    },
    buttonTextStyle: {
      color: 'white',

    },
    item: {
      backgroundColor: 'white',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 16,
    },
    title: {
      fontSize: 16,
      textAlign: 'center',
      fontWeight: 'bold'
    },
      
  });

export default ListAppointments;