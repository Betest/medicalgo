import React, { useState } from 'react';
import { StyleSheet, Text, View, Alert } from 'react-native';
import { TextInput, TouchableHighlight } from 'react-native-gesture-handler';


import  DateTimePickerModal  from  'react-native-modal-datetime-picker';


function CreateAppointment({ navigation }) {

  //datapicker

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [dateAppointment, setDateAppointment] = useState("");
  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);    
  };

  const handleConfirm = (dateAppointment) => {        
    hideDatePicker();
    Alert.alert("Has selecciona la fecha para la cita:", String(dateAppointment));
    setDateAppointment(dateAppointment);
  };
  
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [ident, setIdent] = useState("");
  const [date, setDate] = useState("");
  const [city, setCity] = useState("");
  const [neighborhood, setNeighborhood] = useState("");
  const [mobile, setMobile] = useState("");


  const createAppointment = async () => {
    if (ident == "" || firstName == "" || lastName == "" || city == "" || date == "" || neighborhood == "" || mobile == "") {
      Alert.alert("Debe llenar todos los campos");
    }
    else if (isNaN(ident)) {
      Alert.alert("La identificacion debe ser un número");

    } else if (isNaN(mobile)) {
      Alert.alert("El número de telefono debe ser númerico");

    } else if (mobile.length > 10) {
      Alert.alert("El número debe tener maximo 10 caracteres");

    } else {

      try {
        const response = await fetch('http://192.168.1.60:3000/api/addAppointment', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-type': 'application/json'
          },
          body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            ident: ident,
            date: date,
            city: city,
            neighborhood: neighborhood,
            mobile: mobile,
            dateAppointment: dateAppointment
          })
        });
        const json = await response.json();
        Alert.alert("Cita medica Agendada");
        navigation.navigate("MedicalGo");

      } catch (error) {
        console.log(error);
      }
    }
  }

  return (
    <View style={styles.container}>
      <TextInput style={styles.textInput} placeholder={"Nombre"} onChangeText={text => setFirstName(text)}></TextInput>
      <TextInput style={styles.textInput} placeholder={"Apellido"} onChangeText={text => setLastName(text)}></TextInput>
      <TextInput style={styles.textInput} placeholder={"Identificacion"} onChangeText={text => setIdent(text)} keyboardType={'numeric'}></TextInput>
      <TextInput style={styles.textInput} placeholder={"Fecha Nacimiento"} onChangeText={text => setDate(text)}></TextInput>
      <TextInput style={styles.textInput} placeholder={"Ciudad"} onChangeText={text => setCity(text)}></TextInput>
      <TextInput style={styles.textInput} placeholder={"Barrio"} onChangeText={text => setNeighborhood(text)}></TextInput>
      <TextInput style={styles.textInput} placeholder={"Celular"} onChangeText={text => setMobile(text)} keyboardType={'numeric'}></TextInput>
      <TouchableHighlight style={styles.dateAppointmentButton} onPress = {showDatePicker} >
        <Text style={{color: 'white', justifyContent:'center', alignItems:'center'}}>Fecha Cita</Text>
        </TouchableHighlight>
        < DateTimePickerModal
            isVisible = {isDatePickerVisible}
            mode = {'datetime'} 
            onConfirm = {handleConfirm}
            onCancel = {hideDatePicker}
            is24Hour={false}
        />  
      
      <TouchableHighlight style={styles.createAppointmentButton} onPress={createAppointment}>
        <Text style={styles.textStyleButton}>Crear Cita</Text>
      </TouchableHighlight>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 20,
    borderRadius: 45,
    borderWidth: 2,
    padding: 20,
    backgroundColor: 'white'
  },
  textInput: {
    marginLeft: 40,
    borderRadius: 45,
    color: 'black'
  },
  textStyleButton: {
    fontSize: 16,
    color: 'black'
  },
  createAppointmentButton: {
    alignItems: 'center',
    backgroundColor: '#78ffd6',
    padding: 10,
    borderRadius: 45,
    marginVertical: 8,
    marginHorizontal: 80,
  },  
  dateAppointmentButton: {
    alignItems: 'center',
    backgroundColor: '#dd3e54',
    padding: 10,
    borderRadius: 45,
    marginVertical: 8,
    marginHorizontal: 80,
  },
});
export default CreateAppointment;